module.exports = {
    app      : {
        lang      : 'ru',
        stylus    : {
            theme_color : '#f74236'
        },
        GA        : false, // Google Analytics site's ID
        package   : 'ключ перезаписывается значениями из package.json marmelad-сборщика',
        settings  : 'ключ перезаписывается значениями из файла настроек settings.marmelad.js',
        storage   : 'ключ перезаписывается значениями из файла настроек settings.marmelad.js',
        buildTime : '',
        controls : [
            'default',
            'brand',
            'success',
            'info',
            'warning',
            'danger'
        ]
    },
    pageTitle: 'marmelad',
    socials: [
        "img/social-icon1.png",
        "img/social-icon3.png",
        "img/social-icon2.png"
    ],
    gridBlocks: [
        {
            image: "img/grid-block-image1.png",
            mod: "",
            dayShort: "ПН",
            dayDetail: "24 <br>Сентября <br>2018",
            title: "Звезды российского джаза. Карина Кожевникова",
            time: "13:30, 15:00, 16:30, 18:00, 19:30",
            timeMobile: "24 Янв, 17:35"
        },
        {
            image: "img/grid-block-image1.png",
            mod: "image",
            dayShort: "ВТ",
            dayDetail: "24 <br>Сентября <br>2018",
            title: "Звезды российского джаза. Карина Кожевникова",
            time: "13:30, 15:00, 16:30, 18:00, 19:30",
            timeMobile: "24 Янв, 17:35"
        },
        {
            image: "img/grid-block-image1.png",
            mod: "",
            dayShort: "ЧТ",
            dayDetail: "24 <br>Сентября <br>2018",
            title: "Звезды российского джаза. Карина Кожевникова",
            time: "13:30, 15:00, 16:30, 18:00, 19:30",
            timeMobile: "24 Янв, 17:35"
        },

        {
            image: "img/grid-block-image2.png",
            mod: "",
            dayShort: "ПН",
            dayDetail: "24 <br>Сентября <br>2018",
            title: "Звезды российского джаза. Карина Кожевникова",
            time: "13:30, 15:00, 16:30, 18:00, 19:30",
            timeMobile: "24 Янв, 17:35"
        },
        {
            image: "img/grid-block-image2.png",
            mod: "image",
            dayShort: "ПН",
            dayDetail: "24 <br>Сентября <br>2018",
            title: "Звезды российского джаза. Карина Кожевникова",
            time: "13:30, 15:00, 16:30, 18:00, 19:30",
            timeMobile: "24 Янв, 17:35"
        },
        {
            image: "img/grid-block-image2.png",
            mod: "",
            dayShort: "ПН",
            dayDetail: "24 <br>Сентября <br>2018",
            title: "Звезды российского джаза. Карина Кожевникова",
            time: "13:30, 15:00, 16:30, 18:00, 19:30",
            timeMobile: "24 Янв, 17:35"
        },


        {
            image: "img/grid-block-image3.png",
            mod: "",
            dayShort: "ПН",
            dayDetail: "24 <br>Сентября <br>2018",
            title: "Звезды российского джаза. Карина Кожевникова",
            time: "13:30, 15:00, 16:30, 18:00, 19:30",
            timeMobile: "24 Янв, 17:35"
        },
        {
            image: "img/grid-block-image3.png",
            mod: "image",
            dayShort: "ПН",
            dayDetail: "24 <br>Сентября <br>2018",
            title: "Звезды российского джаза. Карина Кожевникова",
            time: "13:30, 15:00, 16:30, 18:00, 19:30",
            timeMobile: "24 Янв, 17:35"
        },
        {
            image: "img/grid-block-image3.png",
            mod: "",
            dayShort: "ПН",
            dayDetail: "24 <br>Сентября <br>2018",
            title: "Звезды российского джаза. Карина Кожевникова",
            time: "13:30, 15:00, 16:30, 18:00, 19:30",
            timeMobile: "24 Янв, 17:35"
        },
    ]
};
