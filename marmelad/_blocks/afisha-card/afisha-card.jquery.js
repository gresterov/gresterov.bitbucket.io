var originalSlider = $('._js-original-slider').addClass('owl-carousel').owlCarousel({
    loop: true,
    items: 1,
    margin: 0,
    nav: false,
    dots: false,
    mouseDrag: false,
    responsive:{
        0 :{
            nav: true
        },
        451 :{
            nav: false
        }
    },
});

var thumbnailsSlider = $('._js-thumbnails-slider').owlCarousel({
    loop: true,
    margin: 19,
    nav: true,
    responsive:{
        0 :{
            items: 3
        },
        600 :{
            items: 3
        },
        1000 :{
            items: 3
        }
    },
    onInitialized: function() {
        $('._js-to-original-slide').on('click', function(event){
            event.preventDefault();

            var indx = $(this).data('slide-index');

            $('._js-to-original-slide')
                .removeClass('is-active')
                .filter('[data-slide-index="' + indx + '"]')
                .addClass('is-active');

            originalSlider.trigger('to.owl.carousel', indx);
        });
    }
});


var afishaVideoPopup = $('.remodal--afisha-video');

if($.exists(afishaVideoPopup)){
    afishaVideoPopup = afishaVideoPopup.remodal();

    $('._js-open-event-video').on('click', function(event){
        event.preventDefault();
        afishaVideoPopup.open();
    });

    $(document).on('closed', afishaVideoPopup, function(){
        var frame = $('.remodal--afisha-video iframe');
        var src = frame.attr('src');

        frame.attr('src', '').attr('src', src);
    });
}


var descFlag = 1;

function moveDesc() {
    if($(window).width() <= 950 && descFlag == 1) {
        $('.afisha-card__desc-bot-container').appendTo('.afisha-card__container')
        descFlag = 2
    } else if($(window).width() > 950 && descFlag == 2) {
        $('.afisha-card__desc-bot-container').appendTo('.afisha-card__desc-container')
        descFlag = 1
    }
}

$(window).on('resize', moveDesc).trigger('resize');