$('._js-calendar-arrow').on('click', function(event){
    event.preventDefault();

    var $parent = $(this).closest('.event-calendar__items');
    var $list = $parent.find('.event-calendar__list');

    // var step = window.matchMedia('(max-width: 480px)').matches ? 80 : 170;
    var step = $list.width();
    var scrollLeft = $list.scrollLeft() + step;

    if($list.width() + $list.scrollLeft() === $list[0].scrollWidth) {
        scrollLeft = 0;
    }

    $list
        .stop()
        .animate({
            scrollLeft: scrollLeft
        });
});