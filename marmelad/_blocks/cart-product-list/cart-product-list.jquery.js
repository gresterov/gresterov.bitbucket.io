$('select').each(function(){
	var self = $(this);
	var isFilterSelect = $(this).hasClass('filter__select');
	self.select2({
		placeholder: self.data('placeholder'),
		width: false,
		minimumResultsForSearch: -1,
		dropdownCssClass: isFilterSelect ? "filter-select-dropdown" : ""
	});
});