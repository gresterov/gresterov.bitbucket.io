;(function () {
    $('.content-menu').addClass('owl-carousel').owlCarousel({
        items: 4,
        autoplay: false,
        nav: true,
        loop: false,
        dots: false,
        navText: ['',''],
        responsive:{
            0 :{
                items: 1
            },
            380 :{
                items: 2
            },
            768 :{
                items: 4
            }
        }
    })
})();