;(function () {
    var bgCover = $('[data-section-bg]');

    if (!$.exists(bgCover)) {
        return;
    }

    var cover = function cover() {
        var self = $(this);
        var selfBg = self.data('section-bg');

        if (selfBg) {
            self.css('background-image', 'url(' + selfBg + ')');
        }
    };

    $.each(bgCover, cover);
})();

;(function () {
    var mainSlider = $('._js_main-slider');

    if(!$.exists(mainSlider)) {
        return;
    }


    mainSlider.addClass('owl-carousel').owlCarousel({
        items: 1,
        autoplay: true,
        nav: true,
        loop: true,
        dots: true,
        navText: ['','']
    })
})();
