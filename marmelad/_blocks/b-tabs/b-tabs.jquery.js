;(function(){
    var $titles = $('._js-tabs-title');
    var $bodys = $('._js-tabs-body');

    if(!$titles.length) {
        return;
    }

    $titles.on('click', function(event){
        event.preventDefault();

        if($(this).hasClass('is-active')) {
            return;
        }

        var indx = $(this).data('tab-index');

        $titles.removeClass('is-active');
        $(this).addClass('is-active');

        $bodys
            .removeClass('is-active')
            .hide()
            .filter('[data-tab-index="' + indx + '"]')
            .fadeIn();

        if($(this).closest('.b-tabs--afisha-tabs').length && window.matchMedia('(max-width: 730px)').matches) {
            setTimeout(function(){
                $('html,body').animate({
                    scrollTop: $('.b-tabs__container').offset().top - $('.app-header').height()
                });
            }, 400);
        }
    });
})();