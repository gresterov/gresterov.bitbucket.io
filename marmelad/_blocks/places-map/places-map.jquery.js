
// Карта выбора мест
;(function(){
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(window.navigator.userAgent); // мобилка

    function Places(options) {
        var scale = 1;

        // места. превью и оригинал
        var places = { 
            preview : {
                $element : $(options.previewElement),
                $image : $(options.previewImage)
            },
            original : {
                $zoom : $(options.originalZoom),
                $element : $(options.originalElement), 
                $image : $(options.originalImage)
            }
        }

        // панель увеличения
        var zoomPanel = { 
            $element : $(options.zoomPanelElement)
        };

        
        // скроллер мест
        var scroller = { 
            $rows : $(options.scrollerRows),  // ряды
            $image : $(options.scrollerImage) // оригинал изображения
        };

        // задаем размеры элементов
        function setSizes() { 
            // размеры превью и оригинала
            // определение их координал
            for(var key in places) { 
                places[key]._width = places[key].$element.width();
                places[key]._height = places[key].$element.height();

                places[key]._startOffsetX = places[key].$element.offset().left;
                places[key]._startOffsetY = places[key].$element.offset().top;
                places[key]._endOffsetX = places[key]._startOffsetX + places[key]._width;
                places[key]._endOffsetY = places[key]._startOffsetY + places[key]._height;
            }

            // коэффициент масштабирования
            scale = places.preview.$image.width() / places.original.$image.width(); 

            // ширина, высота зум-панели
            zoomPanel._width = places.original._width * scale; 
            zoomPanel._height = places.original._height * scale;

            // задаем размеры зум-панели
            zoomPanel.$element.css({ 
                width: zoomPanel._width,
                height: zoomPanel._height 
            });
        }

        // определение точек прикосновения
        function getXY(event) {
            return {
                X: event.pageX,
                Y: event.pageY
            };
        }

        // проверка точек прикосновения над объектом
        function overTaget(place, touchPoints) {
            if( touchPoints.X >= places[place]._startOffsetX &&
                touchPoints.X <= places[place]._endOffsetX &&
                touchPoints.Y >= places[place]._startOffsetY &&
                touchPoints.Y <= places[place]._endOffsetY ) {

                return true;
            }
        }

        // позиционирование зум-панели
        function moveZoomPanel(event) { 
            var touchPoints = getXY(event);

            var left = touchPoints.X - places.preview._startOffsetX;
            var top = touchPoints.Y - places.preview._startOffsetY;

            var left = left - zoomPanel._width / 2;
            var top = top - zoomPanel._height / 2;

            var forceleft = places.preview._width - zoomPanel._width;
            var forcetop = places.preview._height - zoomPanel._height;
           
            // не выходим за рамки 
            left = left > 0 ? left : 0;
            top = top > 0 ? top : 0;
            left = left + zoomPanel._width < places.preview._width ? left : forceleft;
            top = top + zoomPanel._height < places.preview._height ? top : forcetop; 

            // смещаем зум-панель
            zoomPanel.$element.css({ 
                left: left,
                top: top
            });

            // для оригинала избражения
            left = (left / scale);
            top = (top / scale);

            // смещаем оригинал изображения
            places.original.$element[0].scrollTo({
                left: left,
                top: top,
            });

            // смещаем ряды
            scroller.$rows[0].scrollTo({
                top: top,
            });
        }

        // позиционирование оригинала по движению мыши/тача
        function dragOriginalImage(points) {
            var touchPoints = getXY(event);
            var left = -1 * (touchPoints.X - places.original._startOffsetX - points.startX) + points.scrollLeft;
            var top = -1 *  (touchPoints.Y - places.original._startOffsetY - points.startY) + points.scrollTop;

            // смещаем оригинал изображения
            places.original.$element[0].scroll({
                left: left,
                top: top
            });

            // смещаем ряды
            scroller.$rows[0].scrollTo({
                top: top
            });

            // для зумпанели
            var forceleft = places.preview._width - zoomPanel._width;
            var forcetop = places.preview._height - zoomPanel._height;

            left = left * scale;
            top = top * scale;
           
            // не выходим за рамки 
            left = left > 0 ? left : 0;
            top = top > 0 ? top : 0;
            left = left + zoomPanel._width < places.preview._width ? left : forceleft;
            top = top + zoomPanel._height < places.preview._height ? top : forcetop; 

            // смещаем зум-панель
            zoomPanel.$element.css({ 
                left: left,
                top: top
            });
        }

        // по скролу оригинала смещаем превью
        places.original.$element.on('scroll', function(){
            if(!isMobile) {
                return;
            }

            var scrollLeft = places.original.$element.scrollLeft();
            var scrollTop = places.original.$element.scrollTop();

            // смещаем ряды
            scroller.$rows[0].scrollTo({
                top: scrollTop
            });

            // смещаем зум-панель
            zoomPanel.$element.css({
                left: scrollLeft * scale,
                top: scrollTop * scale
            });
        });

        // по клику по превью отображаем зум-панель и оригинал изображения
        places.preview.$element.on('click', function(event){
            event.preventDefault();

            $('.places__main').addClass('is-active-zoom');
            console.log($(this));

            if($(this).hasClass('_js-preview-place')) {
                $('.places__main').removeClass('is-active-place2');
                $('.places__main').addClass('is-active-place');
            }

            if($(this).hasClass('_js-preview-place2')) {
                $('.places__main').removeClass('is-active-place');
                $('.places__main').addClass('is-active-place2')
            }
        });

        // по клику по превью пересчитываем размеры элементов
        places.preview.$element.one('click', setSizes);

        // сдвигаем зум-панель по клику если
        // диапазон координат точек клика совпадают с координатами превью
        $(document).on('click', function(event) {
            if(!overTaget('preview', getXY(event)) || isMobile) {
                return;
            }
            moveZoomPanel(event);
        });

        // инициализируем сдвиг
        // оригинала или превью изображения по нажатию мыши
        $(document).on('mousedown', function(event){
            var touchPoints = getXY(event);

            // это оригинал изображения
            if(overTaget('original', touchPoints)) {
                var points = {
                    startX : touchPoints.X - places.original._startOffsetX,
                    startY : touchPoints.Y - places.original._startOffsetY,
                    scrollLeft : places.original.$element.scrollLeft(),
                    scrollTop : places.original.$element.scrollTop()

                    // // инвертирование 
                    // startX : touchPoints.X - places.original._startOffsetX,
                    // startY : touchPoints.Y - places.original._startOffsetY,
                    // scrollLeft : places.original.$element.scrollLeft(),
                    // scrollTop : places.original.$element.scrollTop()
                }

                scroller.$image.addClass('cursor-grabbing');
                $(document).on('mousemove', dragOriginalImage.bind(this, points));
            }

            // это превью
            if(overTaget('preview', touchPoints)) {
                zoomPanel.$element.addClass('no-transition');
                places.preview.$element.on('mousemove', moveZoomPanel);
            }
        });

        // на mouseup отменяем все манипуляции с зумпанелью и оригиналом изображения
        $(document).on('mouseup', function(){
            scroller.$image.removeClass('cursor-grabbing');
            zoomPanel.$element.removeClass('no-transition');

            $(document).off('mousemove');
            places.preview.$element.off('mousemove', moveZoomPanel);
        });

        // пересчет размеров на ресайз
        $(window).on('resize', setSizes).trigger('resize');
    }


    // малый зал, партер
    if($('.places').length) {
        var places1 = new Places({
            // превью
            previewElement : '._js-preview-place',
            previewImage : '._js-preview-image',

            // оригинал
            originalZoom : '._js-zoom-place',
            originalElement : '._js-original-place',
            originalImage : '._js-original-image',

            // зум-панель
            zoomPanelElement: '._js-zoom-panel',

            // скроллер оригинала
            scrollerRows: '._js-rows-scroller',
            scrollerImage: '._js-image-scroller'
        });    
    }

    // балкон
    if($('.places--big-hall').length) {
        var places2 = new Places({
            // превью
            previewElement : '._js-preview-place2',
            previewImage : '._js-preview-image2',

            // оригинал
            originalZoom : '._js-zoom-place2',
            originalElement : '._js-original-place2',
            originalImage : '._js-original-image2',

            // зум-панель
            zoomPanelElement: '._js-zoom-panel2',

            // скроллер оригинала
            scrollerRows: '._js-rows-scroller2',
            scrollerImage: '._js-image-scroller2'
        });
    }

})();