$('.scroll-top').on('click', function(e) {
    $('html, body').animate({
       scrollTop: 0
    });
});


$(window).on('scroll', function(){
    if($(window).scrollTop() > 100) {
        $('.scroll-top').addClass('is-visible');
    } else {
        $('.scroll-top').removeClass('is-visible');
    }
}).trigger('scroll');