$('._js-mobile-nav-open').on('click', function(){
	$(this).toggleClass('is-active');
	$('.fix-mob-navigation').toggleClass('is-opened');
});

$(window).on('resize', function(){
	if(window.matchMedia('(min-width: 731px').matches) {
		if($('.fix-mob-navigation.is-opened').length) {
			$('.fix-mob-navigation').removeClass('is-opened');
			$('._js-mobile-nav-open').removeClass('is-active');
		}
	}
});