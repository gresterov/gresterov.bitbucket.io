'use strict';

/**
 * Подключение JS файлов которые начинаются с подчеркивания
 */
/**
 * Возвращает функцию, которая не будет срабатывать, пока продолжает вызываться.
 * Она сработает только один раз через N миллисекунд после последнего вызова.
 * Если ей передан аргумент `immediate`, то она будет вызвана один раз сразу после
 * первого запуска.
 */
function debounce(func, wait, immediate) {

    var timeout = null,
        context = null,
        args = null,
        later = null,
        callNow = null;

    return function () {

        context = this;
        args = arguments;

        later = function later() {

            timeout = null;
            if (!immediate) {
                func.apply(context, args);
            }
        };
        callNow = immediate && !timeout;

        clearTimeout(timeout);
        timeout = setTimeout(later, wait);

        if (callNow) {
            func.apply(context, args);
        }
    };
}

// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
// MIT license

;(function () {
    var lastTime = 0,
        vendors = ['ms', 'moz', 'webkit', 'o'],
        x = void 0,
        currTime = void 0,
        timeToCall = void 0,
        id = void 0;

    for (x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame) {

        window.requestAnimationFrame = function (callback) {

            currTime = new Date().getTime();
            timeToCall = Math.max(0, 16 - (currTime - lastTime));
            id = window.setTimeout(function () {
                callback(currTime + timeToCall);
            }, timeToCall);

            lastTime = currTime + timeToCall;

            return id;
        };
    }

    if (!window.cancelAnimationFrame) {

        window.cancelAnimationFrame = function (id) {
            clearTimeout(id);
        };
    }
})();
;(function () {

    // Test via a getter in the options object to see if the passive property is accessed

    var supportsPassiveOpts = null;

    try {
        supportsPassiveOpts = Object.defineProperty({}, 'passive', {
            get: function get() {
                window.supportsPassive = true;
            }
        });
        window.addEventListener('est', null, supportsPassiveOpts);
    } catch (e) {}

    // Use our detect's results. passive applied if supported, capture will be false either way.
    //elem.addEventListener('touchstart', fn, supportsPassive ? { passive: true } : false);
})();
function getSVGIconHTML(name, tag, attrs) {

    if (typeof name === 'undefined') {
        console.error('name is required');
        return false;
    }

    if (typeof tag === 'undefined') {
        tag = 'div';
    }

    var classes = 'svg-icon svg-icon--<%= name %>';

    var iconHTML = ['<<%= tag %> <%= classes %>>', '<svg class="svg-icon__link">', '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#<%= name %>"></use>', '</svg>', '</<%= tag %>>'].join('').replace(/<%= classes %>/g, 'class="' + classes + '"').replace(/<%= tag %>/g, tag).replace(/<%= name %>/g, name);

    return iconHTML;
}

/* ^^^
 * JQUERY Actions
 * ========================================================================== */
$(function () {

    'use strict';

    /**
     * определение существования элемента на странице
     */

    $.exists = function (selector) {
        return $(selector).length > 0;
    };

    /**
     * [^_]*.js - выборка всех файлов, которые не начинаются с подчеркивания
     */

    $('._js-show-tablet-nav').on('click', function (event) {
        event.preventDefault();

        $(this).toggleClass('is-active');
        $('.app-header__nav--tablet').toggleClass('is-opened');
    });
    var originalSlider = $('._js-original-slider').addClass('owl-carousel').owlCarousel({
        loop: true,
        items: 1,
        margin: 0,
        nav: false,
        dots: false,
        mouseDrag: false,
        responsive: {
            0: {
                nav: true
            },
            451: {
                nav: false
            }
        }
    });

    var thumbnailsSlider = $('._js-thumbnails-slider').owlCarousel({
        loop: true,
        margin: 19,
        nav: true,
        responsive: {
            0: {
                items: 3
            },
            600: {
                items: 3
            },
            1000: {
                items: 3
            }
        },
        onInitialized: function onInitialized() {
            $('._js-to-original-slide').on('click', function (event) {
                event.preventDefault();

                var indx = $(this).data('slide-index');

                $('._js-to-original-slide').removeClass('is-active').filter('[data-slide-index="' + indx + '"]').addClass('is-active');

                originalSlider.trigger('to.owl.carousel', indx);
            });
        }
    });

    var afishaVideoPopup = $('.remodal--afisha-video');

    if ($.exists(afishaVideoPopup)) {
        afishaVideoPopup = afishaVideoPopup.remodal();

        $('._js-open-event-video').on('click', function (event) {
            event.preventDefault();
            afishaVideoPopup.open();
        });

        $(document).on('closed', afishaVideoPopup, function () {
            var frame = $('.remodal--afisha-video iframe');
            var src = frame.attr('src');

            frame.attr('src', '').attr('src', src);
        });
    }

    var descFlag = 1;

    function moveDesc() {
        if ($(window).width() <= 950 && descFlag == 1) {
            $('.afisha-card__desc-bot-container').appendTo('.afisha-card__container');
            descFlag = 2;
        } else if ($(window).width() > 950 && descFlag == 2) {
            $('.afisha-card__desc-bot-container').appendTo('.afisha-card__desc-container');
            descFlag = 1;
        }
    }

    $(window).on('resize', moveDesc).trigger('resize');
    ;(function () {
        var $titles = $('._js-tabs-title');
        var $bodys = $('._js-tabs-body');

        if (!$titles.length) {
            return;
        }

        $titles.on('click', function (event) {
            event.preventDefault();

            if ($(this).hasClass('is-active')) {
                return;
            }

            var indx = $(this).data('tab-index');

            $titles.removeClass('is-active');
            $(this).addClass('is-active');

            $bodys.removeClass('is-active').hide().filter('[data-tab-index="' + indx + '"]').fadeIn();

            if ($(this).closest('.b-tabs--afisha-tabs').length && window.matchMedia('(max-width: 730px)').matches) {
                setTimeout(function () {
                    $('html,body').animate({
                        scrollTop: $('.b-tabs__container').offset().top - $('.app-header').height()
                    });
                }, 400);
            }
        });
    })();
    ;(function () {
        var bgCover = $('[data-section-bg]');

        if (!$.exists(bgCover)) {
            return;
        }

        var cover = function cover() {
            var self = $(this);
            var selfBg = self.data('section-bg');

            if (selfBg) {
                self.css('background-image', 'url(' + selfBg + ')');
            }
        };

        $.each(bgCover, cover);
    })();

    ;(function () {
        var mainSlider = $('._js_main-slider');

        if (!$.exists(mainSlider)) {
            return;
        }

        mainSlider.addClass('owl-carousel').owlCarousel({
            items: 1,
            autoplay: true,
            nav: true,
            loop: true,
            dots: true,
            navText: ['', '']
        });
    })();

    $('select').each(function () {
        var self = $(this);
        var isFilterSelect = $(this).hasClass('filter__select');
        self.select2({
            placeholder: self.data('placeholder'),
            width: false,
            minimumResultsForSearch: -1,
            dropdownCssClass: isFilterSelect ? "filter-select-dropdown" : ""
        });
    });
    ;(function () {
        $('.content-menu').addClass('owl-carousel').owlCarousel({
            items: 4,
            autoplay: false,
            nav: true,
            loop: false,
            dots: false,
            navText: ['', ''],
            responsive: {
                0: {
                    items: 1
                },
                380: {
                    items: 2
                },
                768: {
                    items: 4
                }
            }
        });
    })();
    $('._js-calendar-arrow').on('click', function (event) {
        event.preventDefault();

        var $parent = $(this).closest('.event-calendar__items');
        var $list = $parent.find('.event-calendar__list');

        // var step = window.matchMedia('(max-width: 480px)').matches ? 80 : 170;
        var step = $list.width();
        var scrollLeft = $list.scrollLeft() + step;

        if ($list.width() + $list.scrollLeft() === $list[0].scrollWidth) {
            scrollLeft = 0;
        }

        $list.stop().animate({
            scrollLeft: scrollLeft
        });
    });
    $('._js-select-trigger-click').on('click', function () {
        $(this).closest('.filter__item').find('.select2-selection').trigger('click');
    });
    $('._js-mobile-nav-open').on('click', function () {
        $(this).toggleClass('is-active');
        $('.fix-mob-navigation').toggleClass('is-opened');
    });

    $(window).on('resize', function () {
        if (window.matchMedia('(min-width: 731px').matches) {
            if ($('.fix-mob-navigation.is-opened').length) {
                $('.fix-mob-navigation').removeClass('is-opened');
                $('._js-mobile-nav-open').removeClass('is-active');
            }
        }
    });

    // Карта выбора мест
    ;(function () {
        var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(window.navigator.userAgent); // мобилка

        function Places(options) {
            var scale = 1;

            // места. превью и оригинал
            var places = {
                preview: {
                    $element: $(options.previewElement),
                    $image: $(options.previewImage)
                },
                original: {
                    $zoom: $(options.originalZoom),
                    $element: $(options.originalElement),
                    $image: $(options.originalImage)
                }

                // панель увеличения
            };var zoomPanel = {
                $element: $(options.zoomPanelElement)
            };

            // скроллер мест
            var scroller = {
                $rows: $(options.scrollerRows), // ряды
                $image: $(options.scrollerImage) // оригинал изображения
            };

            // задаем размеры элементов
            function setSizes() {
                // размеры превью и оригинала
                // определение их координал
                for (var key in places) {
                    places[key]._width = places[key].$element.width();
                    places[key]._height = places[key].$element.height();

                    places[key]._startOffsetX = places[key].$element.offset().left;
                    places[key]._startOffsetY = places[key].$element.offset().top;
                    places[key]._endOffsetX = places[key]._startOffsetX + places[key]._width;
                    places[key]._endOffsetY = places[key]._startOffsetY + places[key]._height;
                }

                // коэффициент масштабирования
                scale = places.preview.$image.width() / places.original.$image.width();

                // ширина, высота зум-панели
                zoomPanel._width = places.original._width * scale;
                zoomPanel._height = places.original._height * scale;

                // задаем размеры зум-панели
                zoomPanel.$element.css({
                    width: zoomPanel._width,
                    height: zoomPanel._height
                });
            }

            // определение точек прикосновения
            function getXY(event) {
                return {
                    X: event.pageX,
                    Y: event.pageY
                };
            }

            // проверка точек прикосновения над объектом
            function overTaget(place, touchPoints) {
                if (touchPoints.X >= places[place]._startOffsetX && touchPoints.X <= places[place]._endOffsetX && touchPoints.Y >= places[place]._startOffsetY && touchPoints.Y <= places[place]._endOffsetY) {

                    return true;
                }
            }

            // позиционирование зум-панели
            function moveZoomPanel(event) {
                var touchPoints = getXY(event);

                var left = touchPoints.X - places.preview._startOffsetX;
                var top = touchPoints.Y - places.preview._startOffsetY;

                var left = left - zoomPanel._width / 2;
                var top = top - zoomPanel._height / 2;

                var forceleft = places.preview._width - zoomPanel._width;
                var forcetop = places.preview._height - zoomPanel._height;

                // не выходим за рамки 
                left = left > 0 ? left : 0;
                top = top > 0 ? top : 0;
                left = left + zoomPanel._width < places.preview._width ? left : forceleft;
                top = top + zoomPanel._height < places.preview._height ? top : forcetop;

                // смещаем зум-панель
                zoomPanel.$element.css({
                    left: left,
                    top: top
                });

                // для оригинала избражения
                left = left / scale;
                top = top / scale;

                // смещаем оригинал изображения
                places.original.$element[0].scrollTo({
                    left: left,
                    top: top
                });

                // смещаем ряды
                scroller.$rows[0].scrollTo({
                    top: top
                });
            }

            // позиционирование оригинала по движению мыши/тача
            function dragOriginalImage(points) {
                var touchPoints = getXY(event);
                var left = -1 * (touchPoints.X - places.original._startOffsetX - points.startX) + points.scrollLeft;
                var top = -1 * (touchPoints.Y - places.original._startOffsetY - points.startY) + points.scrollTop;

                // смещаем оригинал изображения
                places.original.$element[0].scroll({
                    left: left,
                    top: top
                });

                // смещаем ряды
                scroller.$rows[0].scrollTo({
                    top: top
                });

                // для зумпанели
                var forceleft = places.preview._width - zoomPanel._width;
                var forcetop = places.preview._height - zoomPanel._height;

                left = left * scale;
                top = top * scale;

                // не выходим за рамки 
                left = left > 0 ? left : 0;
                top = top > 0 ? top : 0;
                left = left + zoomPanel._width < places.preview._width ? left : forceleft;
                top = top + zoomPanel._height < places.preview._height ? top : forcetop;

                // смещаем зум-панель
                zoomPanel.$element.css({
                    left: left,
                    top: top
                });
            }

            // по скролу оригинала смещаем превью
            places.original.$element.on('scroll', function () {
                if (!isMobile) {
                    return;
                }

                var scrollLeft = places.original.$element.scrollLeft();
                var scrollTop = places.original.$element.scrollTop();

                // смещаем ряды
                scroller.$rows[0].scrollTo({
                    top: scrollTop
                });

                // смещаем зум-панель
                zoomPanel.$element.css({
                    left: scrollLeft * scale,
                    top: scrollTop * scale
                });
            });

            // по клику по превью отображаем зум-панель и оригинал изображения
            places.preview.$element.on('click', function (event) {
                event.preventDefault();

                $('.places__main').addClass('is-active-zoom');
                console.log($(this));

                if ($(this).hasClass('_js-preview-place')) {
                    $('.places__main').removeClass('is-active-place2');
                    $('.places__main').addClass('is-active-place');
                }

                if ($(this).hasClass('_js-preview-place2')) {
                    $('.places__main').removeClass('is-active-place');
                    $('.places__main').addClass('is-active-place2');
                }
            });

            // по клику по превью пересчитываем размеры элементов
            places.preview.$element.one('click', setSizes);

            // сдвигаем зум-панель по клику если
            // диапазон координат точек клика совпадают с координатами превью
            $(document).on('click', function (event) {
                if (!overTaget('preview', getXY(event)) || isMobile) {
                    return;
                }
                moveZoomPanel(event);
            });

            // инициализируем сдвиг
            // оригинала или превью изображения по нажатию мыши
            $(document).on('mousedown', function (event) {
                var touchPoints = getXY(event);

                // это оригинал изображения
                if (overTaget('original', touchPoints)) {
                    var points = {
                        startX: touchPoints.X - places.original._startOffsetX,
                        startY: touchPoints.Y - places.original._startOffsetY,
                        scrollLeft: places.original.$element.scrollLeft(),
                        scrollTop: places.original.$element.scrollTop()

                        // // инвертирование 
                        // startX : touchPoints.X - places.original._startOffsetX,
                        // startY : touchPoints.Y - places.original._startOffsetY,
                        // scrollLeft : places.original.$element.scrollLeft(),
                        // scrollTop : places.original.$element.scrollTop()
                    };

                    scroller.$image.addClass('cursor-grabbing');
                    $(document).on('mousemove', dragOriginalImage.bind(this, points));
                }

                // это превью
                if (overTaget('preview', touchPoints)) {
                    zoomPanel.$element.addClass('no-transition');
                    places.preview.$element.on('mousemove', moveZoomPanel);
                }
            });

            // на mouseup отменяем все манипуляции с зумпанелью и оригиналом изображения
            $(document).on('mouseup', function () {
                scroller.$image.removeClass('cursor-grabbing');
                zoomPanel.$element.removeClass('no-transition');

                $(document).off('mousemove');
                places.preview.$element.off('mousemove', moveZoomPanel);
            });

            // пересчет размеров на ресайз
            $(window).on('resize', setSizes).trigger('resize');
        }

        // малый зал, партер
        if ($('.places').length) {
            var places1 = new Places({
                // превью
                previewElement: '._js-preview-place',
                previewImage: '._js-preview-image',

                // оригинал
                originalZoom: '._js-zoom-place',
                originalElement: '._js-original-place',
                originalImage: '._js-original-image',

                // зум-панель
                zoomPanelElement: '._js-zoom-panel',

                // скроллер оригинала
                scrollerRows: '._js-rows-scroller',
                scrollerImage: '._js-image-scroller'
            });
        }

        // балкон
        if ($('.places--big-hall').length) {
            var places2 = new Places({
                // превью
                previewElement: '._js-preview-place2',
                previewImage: '._js-preview-image2',

                // оригинал
                originalZoom: '._js-zoom-place2',
                originalElement: '._js-original-place2',
                originalImage: '._js-original-image2',

                // зум-панель
                zoomPanelElement: '._js-zoom-panel2',

                // скроллер оригинала
                scrollerRows: '._js-rows-scroller2',
                scrollerImage: '._js-image-scroller2'
            });
        }
    })();

    $('.scroll-top').on('click', function (e) {
        $('html, body').animate({
            scrollTop: 0
        });
    });

    $(window).on('scroll', function () {
        if ($(window).scrollTop() > 100) {
            $('.scroll-top').addClass('is-visible');
        } else {
            $('.scroll-top').removeClass('is-visible');
        }
    }).trigger('scroll');

    $('._js_filter-show').on('click', function (event) {
        event.preventDefault();
        $(this).toggleClass('is-active');
        $('._js-filter').toggleClass('is-active');
    });
});